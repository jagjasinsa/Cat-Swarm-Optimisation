
import numpy as num
import random as rand
import matplotlib.pyplot as plt

def find_value(arr):
    x = arr**2
    y = num.sum(x,axis =1)
    return y

def find_best_index(pbest):
    array = pbest**2
    x = num.sum(array,axis = 1)
    index = num.argmin(x)
    return index



def seek_mode(seekSize,posVectorSeek,dimNumber):
    temp = num.zeros(shape = (5,dimNumber))
    for i in range(0,seekSize):
        for j in range(0,5):
            temp[j,:] = posVectorSeek[i,:]
            y = num.random.randint(0,dimNumber)
            temp[j,y] = num.round(num.random.rand(),2)
        
        x = find_best_index(temp)
        posVectorSeek[i,:] = temp[x,:]
        temp = num.zeros(shape = (5,dimNumber))
    return posVectorSeek

def trace_mode(seekSize,catNumber,traceSize,posVectorTrace,velVectorTrace,dimNumber):
    a = find_best_index(posVectorTrace)
    gbest = num.zeros(shape = (traceSize,dimNumber))
    for i in range(0,traceSize):
        gbest[i,:] = posVectorTrace[a,:]
    velVectorTrace[seekSize:,:] = velVectorTrace[seekSize:,:] + num.round(num.random.rand(),2)*2*(gbest-posVectorTrace)
    posVectorTrace = posVectorTrace + velVectorTrace[seekSize:,:]
    return velVectorTrace,posVectorTrace
    
    


def find_gen(catNumber,seekSize,traceSize,posVectorSeek,posVectorTrace,velVectorTrace,dimNumber):
    posVectorSeek = seek_mode(seekSize,posVectorSeek,dimNumber)
    velVectorTrace,posVectorTrace = trace_mode(seekSize,catNumber,traceSize,posVectorTrace,velVectorTrace,dimNumber)
    return posVectorSeek,posVectorTrace,velVectorTrace



noc = input("Please mention the total number of cats\n")
catNumber = int(noc)
dimNumber = int(3)

seekSize = int(catNumber*0.8)
traceSize = catNumber - seekSize

posVectorSeek = (num.random.rand(seekSize,dimNumber)-0.5)*10.24
posVectorTrace = (num.random.rand(traceSize,dimNumber)-0.5)*10.24
velVectorTrace = num.random.rand(catNumber,dimNumber)

c = num.concatenate((posVectorSeek,posVectorTrace),axis = 0)
plt.plot(find_value(c),'r-',label='1st Gen')
print(find_value(c))
for i in range(0,10000):
    posVectorSeek,posVectorTrace,velVectorTrace = find_gen(catNumber,seekSize,traceSize,posVectorSeek,posVectorTrace,velVectorTrace,dimNumber)
    c = num.concatenate((posVectorSeek,posVectorTrace),axis = 0)
    d = num.concatenate((c,velVectorTrace),axis=1)
    num.random.shuffle(d)
    e = d[:,:dimNumber]
    velVectorTrace = d[:,dimNumber:]
    posVectorSeek = e[:seekSize,:]
    posVectorTrace = e[seekSize:,:]
    
c = num.concatenate((posVectorSeek,posVectorTrace),axis = 0)
plt.plot(find_value(c),'b-',label='Last Gen')
plt.plot([-10])
plt.legend()
plt.show
    
    
